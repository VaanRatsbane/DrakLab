﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public partial class AboutForm : Form
    {

        bool _dark
        {
            get { return Program.isDarkMode; }
        }

        public AboutForm()
        {
            InitializeComponent();

            BackColor = _dark ? Program.DarkForm : SystemColors.Control;
            
            label1.ForeColor =
                label2.ForeColor =
                label3.ForeColor =
                label4.ForeColor = _dark ? SystemColors.Control : SystemColors.WindowText;

            linkLabel1.LinkColor =
                linkLabel2.LinkColor =
                linkLabel3.LinkColor =
                linkLabel1.VisitedLinkColor =
                linkLabel2.VisitedLinkColor =
                linkLabel3.VisitedLinkColor = Color.FromArgb(0, _dark ? 165 : 0, 255);

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/topher-au");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/ffgriever");
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://steamcommunity.com/id/Vaan");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
