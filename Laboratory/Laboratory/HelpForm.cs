﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public partial class HelpForm : Form
    {

        private bool _dark { get { return Program.isDarkMode; } }

        public HelpForm()
        {
            InitializeComponent();
            BackColor = _dark ? Program.DarkForm : SystemColors.Control;
            label1.ForeColor =
                label2.ForeColor =
                label3.ForeColor =
                addModLabel.ForeColor =
                applyChangesLabel.ForeColor =
                arrowLabel.ForeColor =
                deleteLabel.ForeColor =
                filesLabel.ForeColor =
                optionsLabel.ForeColor =
                toggleLabel.ForeColor = _dark ? SystemColors.Control : SystemColors.WindowText;

            linkLabel.LinkColor = linkLabel.VisitedLinkColor = Color.FromArgb(0, _dark ? 165 : 0, 255);
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new YoutubeForm()
            {
                Owner = this
            }.ShowDialog();
        }

        private void HelpForm_Resize(object sender, EventArgs e)
        {
            label2.Size = new Size(Size.Width, label2.Size.Height);
            label2.Location = new Point(0, label2.Location.Y);
        }

        private void HelpForm_Load(object sender, EventArgs e)
        {
            HelpForm_Resize(sender, e);
        }

        private void welcomeBtn_Click(object sender, EventArgs e)
        {
            new InitForm()
            {
                Owner = this
            }.ShowDialog();
        }
    }
}
