﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratory
{
    class Scaffold
    {
        //Gets all top level files from active mods
        public static ModFile[] GetScaffold(List<Mod> mods)
        {
            Lab._popup.Message("Calculating files...");
            var list = new List<ModFile>();
            foreach (var m in mods)
                if(m.state == ModState.ACTIVE && m.files.Count > 0)
                {
                    var virtuals = m.GetVirtualFiles();
                    foreach (var v in virtuals)
                        if (list.Count(o => o.virtualPath == v) == 0)
                        {
                            var file = m.GetModFile(v);
                            if (file == null)
                                Program.Log($"Failed to get file {v} belonging to mod {m.modName}.");
                            else
                                list.Add(m.GetModFile(v));
                        }
                }
            return list.ToArray();
        }
    }
}
