﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public partial class InitForm : Form
    {

        private bool _dark { get { return Program.isDarkMode; } }

        public InitForm()
        {
            InitializeComponent();
            BackColor = _dark ? Program.DarkForm : SystemColors.Control;
            label1.ForeColor =
                label2.ForeColor =
                label3.ForeColor =
                label4.ForeColor =
                label5.ForeColor =
                label6.ForeColor =
                label7.ForeColor =
                label8.ForeColor =
                openAtStartCB.ForeColor = _dark ? SystemColors.Control : SystemColors.WindowText;

            tableLayoutPanel3.BackColor = _dark ? Program.DarkTextBG : SystemColors.Control;

            openAtStartCB.Checked = Program.settings.Startup;
        }

        private void openAtStartCB_CheckedChanged(object sender, EventArgs e)
        {
            Program.settings.Startup = openAtStartCB.Checked;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
