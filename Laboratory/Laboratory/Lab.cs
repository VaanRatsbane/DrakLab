﻿using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public partial class Lab : Form
    {

        bool _dark
        {
            get { return Program.isDarkMode; }
        }

        private VistaFolderBrowserDialog addModDialog;
        public static Lab instance;
        public static PopupThread _popup;

        const string FFXIIPNAME = "FFXII_TZA";

        public Lab()
        {
            if (instance != null)
            {
                Close();
            }
            else
            {
                instance = this;
                InitializeComponent();
                _popup = new PopupThread();

                button1.BringToFront();
                if (!(Directory.Exists("ff12-vbf") && File.Exists("ff12-vbf\\ff12-vbf.exe") &&
                    File.Exists("ff12-vbf\\libgcc_s_seh-1.dll") && File.Exists("ff12-vbf\\libstdc++-6.dll") &&
                    File.Exists("ff12-vbf\\libwinpthread-1.dll") && File.Exists("ff12-vbf\\zlib1.dll")))
                {
                    MessageBox.Show("ffgriever's tools are missing. Exiting...");
                    Environment.Exit(1);
                }

                addModDialog = new VistaFolderBrowserDialog()
                {
                    ShowNewFolderButton = false
                };

                openVBFDialog.DefaultExt = "vbf";
                openVBFDialog.Filter = "VirtuosBigFile files (*.vbf)|*.vbf|All files (*.*)|*.*";

                if (!Directory.Exists(Program.settings.LastVBFLocation))
                {
                    var pfiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
                    if (Directory.Exists(pfiles + "\\Steam\\steamapps\\common"))
                        pfiles += "\\Steam\\steamapps\\common";
                    Program.settings.LastVBFLocation = pfiles;
                }

                openVBFDialog.InitialDirectory = Program.settings.LastVBFLocation;

                Recolor();
            }
        }

        private void Lab_Shown(object sender, EventArgs e)
        {
            if (Program.settings.Startup)
            {
                new InitForm()
                {
                    Owner = this
                }.ShowDialog();
            }
        }

        private void Recolor()
        {
            BackColor = _dark ? Program.DarkForm : SystemColors.Control;
            statusLabel.ForeColor = _dark ? SystemColors.Control : SystemColors.ControlText;

            modNameText.BackColor =
                modDescriptionText.BackColor = _dark ? Program.DarkSubForm : SystemColors.Control;

            modNameText.ForeColor =
                modAuthorText.ForeColor =
                modVersionLabel.ForeColor =
                modDescriptionText.ForeColor =
                logLabel.ForeColor = _dark ? SystemColors.Control : SystemColors.WindowText;

            modList.BackColor = _dark ? Program.DarkSubForm : SystemColors.Window;
            modList.ForeColor = _dark ? SystemColors.Control : SystemColors.WindowText;

            foreach (ListViewItem item in modList.Items)
            {
                var mod = Program.manager.GetMod(item.Index);
                switch(mod.state)
                {
                    case ModState.ACTIVE: item.BackColor = _dark ? Color.DarkGreen : Color.LightGreen; break;
                    case ModState.CORRUPTED: item.BackColor = _dark ? Color.DarkOrange : Color.Orange; break;
                    case ModState.INACTIVE: item.BackColor = _dark ? Program.DarkSubForm : Color.White; break;
                    default: break;
                }
            }
            modList.SelectedItems.Clear();
        }

        private void UpdateStatusLabel()
        {
            statusLabel.Text = $"Loaded. {Program.manager.modCount} mods available, {Program.manager.activeCount} mods active.";
        }

        private void loadVBFPicButton_Click(object sender, EventArgs e)
        {
            if (openVBFDialog.ShowDialog() == DialogResult.OK)
            {
                Cursor.Current = Cursors.WaitCursor;

                var folder = Path.GetDirectoryName(openVBFDialog.FileName);
                var folderInfo = new DirectoryInfo(folder);

                if(!folderInfo.IsReadable())
                {
                    MessageBox.Show($"You do not have read permissions on the folder. Try relaunching as Administrator.\n\n${folder}");
                    return;
                }

                if (!folderInfo.IsWriteable())
                {
                    MessageBox.Show($"You do not have write permissions on the folder. Try relaunching as Administrator.\n\n${folder}");
                    return;
                }

                _popup.ShowPopup(Location.X, Location.Y, Width, Height);

                if (LoadVBF(openVBFDialog.FileName))
                {
                    _popup.ClosePopup();
                    UpdateStatusLabel();
                    Program.settings.LastVBFLocation = Path.GetDirectoryName(openVBFDialog.FileName);
                }
                else
                {
                    _popup.ClosePopup();
                    MessageBox.Show("Could not load the VBF.");
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private bool LoadVBF(string filePath)
        {
            _popup.Message("Loading VBF");
            if (Program.LoadVBF(filePath))
            {
                _popup.Message("Loading Mods");
                LoadMods();
                addModButton.Enabled = true;
                addModButton.Image = Properties.Resources.addmod;
                addModButton.BorderStyle = BorderStyle.FixedSingle;

                moveUpButton.Image = Properties.Resources.arrowup;
                moveUpButton.BorderStyle = BorderStyle.FixedSingle;

                moveDownButton.Image = Properties.Resources.arrowdown;
                moveDownButton.BorderStyle = BorderStyle.FixedSingle;
                return true;
            }
            Log("Failed to load the VBF.");
            return false;
        }

        private void LoadMods()
        {
            Program.manager = new ModManager();
            modList.Items.Clear();
            var e = Program.manager.GetEnumerator();
            int i = 0;
            while (e.MoveNext())
            {
                modList.Items.Add(e.Current.GetListItem());
                i++;
            }
            Program.manager.UpdateCurrentScaffold();
            Log($"Loaded {i} mods.");
        }

        private void addModButton_Click(object sender, EventArgs e)
        {
            if (addModDialog.ShowDialog() ?? false)
            {
                Cursor.Current = Cursors.WaitCursor;
                _popup.ShowPopup(Location.X, Location.Y, Width, Height);
                var mod = Program.manager.AddMod(addModDialog.SelectedPath, this);
                _popup.ClosePopup();
                if (mod != null)
                {
                    UpdateStatusLabel();
                    modList.Items.Add(mod.GetListItem());
                    Program.manager.Save();
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void modList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (modList.SelectedItems.Count == 0)
                return;

            if (modList.SelectedItems.Count > 1)
            {
                modDescriptionText.Text = String.Empty;
                modAuthorText.Text = String.Empty;
                modNameText.Text = $"{modList.SelectedItems.Count} mods selected.";
                toolTip1.SetToolTip(modNameText, String.Empty);
                toolTip1.SetToolTip(modAuthorText, String.Empty);
                toolTip1.SetToolTip(modVersionLabel, String.Empty);
                modVersionLabel.Text = String.Empty;
                moveUpButton.Enabled = false;
                moveUpButton.Image = null;
                moveUpButton.BorderStyle = BorderStyle.None;
                moveDownButton.Enabled = false;
                moveDownButton.Image = null;
                moveDownButton.BorderStyle = BorderStyle.None;
                updateModButton.Image = null;
                updateModButton.BorderStyle = BorderStyle.None;
                updateModButton.Enabled = false;
                listFilesButton.Image = null;
                listFilesButton.BorderStyle = BorderStyle.None;
                listFilesButton.Enabled = false;

                var corrupted = false;
                for (int i = 0; i < modList.SelectedIndices.Count; i++)
                    if ((modList.Items[modList.SelectedIndices[i]].Tag as Mod).state == ModState.CORRUPTED)
                    {
                        corrupted = true;
                        break;
                    }

                if(!corrupted)
                {
                    toggleModButton.Image = Properties.Resources.toggle;
                    toggleModButton.BorderStyle = BorderStyle.FixedSingle;
                    toggleModButton.Enabled = true;

                    removeModButton.Image = Properties.Resources.delete1;
                    removeModButton.BorderStyle = BorderStyle.FixedSingle;
                    removeModButton.Enabled = true;
                }
            }
            else
            {
                var index = modList.SelectedItems[0].Index;
                var mod = Program.manager.GetMod(index);
                modDescriptionText.Text = mod.description;
                modAuthorText.Text = $"By {mod.author}";
                modNameText.Text = mod.modName;
                toolTip1.SetToolTip(modNameText, mod.modName);
                toolTip1.SetToolTip(modAuthorText, mod.author);
                toolTip1.SetToolTip(modVersionLabel, mod.version);
                modVersionLabel.Text = $"Version: {mod.version}";
                moveUpButton.Enabled = index > 0;
                moveUpButton.Image = index > 0 ? Properties.Resources.arrowup : null;
                moveUpButton.BorderStyle = index > 0 ? BorderStyle.FixedSingle : BorderStyle.None;
                moveDownButton.Enabled = index < modList.Items.Count - 1;
                moveDownButton.Image = index < modList.Items.Count - 1 ? Properties.Resources.arrowdown : null;
                moveDownButton.BorderStyle = index < modList.Items.Count - 1 ? BorderStyle.FixedSingle : BorderStyle.None;
                switch (mod.state)
                {
                    case ModState.ACTIVE:
                        toggleModButton.Image = Properties.Resources.disable;
                        toggleModButton.BorderStyle = BorderStyle.FixedSingle;
                        toggleModButton.Enabled = true;

                        updateModButton.Image = Properties.Resources.update;
                        updateModButton.BorderStyle = BorderStyle.FixedSingle;
                        updateModButton.Enabled = true;

                        listFilesButton.Image = Properties.Resources.files;
                        listFilesButton.BorderStyle = BorderStyle.FixedSingle;
                        listFilesButton.Enabled = true;
                        break;
                    case ModState.INACTIVE:
                        toggleModButton.Image = Properties.Resources.add;
                        toggleModButton.BorderStyle = BorderStyle.FixedSingle;
                        toggleModButton.Enabled = true;

                        updateModButton.Image = Properties.Resources.update;
                        updateModButton.BorderStyle = BorderStyle.FixedSingle;
                        updateModButton.Enabled = true;

                        listFilesButton.Image = Properties.Resources.files;
                        listFilesButton.BorderStyle = BorderStyle.FixedSingle;
                        listFilesButton.Enabled = true;
                        break;
                    case ModState.CORRUPTED:
                    default:
                        toggleModButton.Image = null;
                        toggleModButton.BorderStyle = BorderStyle.None;
                        toggleModButton.Enabled = false;

                        updateModButton.Image = null;
                        updateModButton.BorderStyle = BorderStyle.None;
                        updateModButton.Enabled = false;

                        listFilesButton.Image = null;
                        listFilesButton.BorderStyle = BorderStyle.None;
                        listFilesButton.Enabled = false;
                        break;
                }
                removeModButton.Enabled = true;
                removeModButton.Image = Properties.Resources.delete1;
                removeModButton.BorderStyle = BorderStyle.FixedSingle;
            }
        }

        private void moveUpButton_Click(object sender, EventArgs e)
        {
            if (modList.SelectedItems.Count == 1)
            {
                var index = modList.SelectedItems[0].Index;
                if (index > 0)
                {
                    TradePlaces(index - 1, index);
                    RecalcRanks();
                }
            }
        }

        private void moveDownButton_Click(object sender, EventArgs e)
        {
            if (modList.SelectedItems.Count == 1)
            {
                var index = modList.SelectedItems[0].Index;
                if (index < modList.Items.Count - 1)
                {
                    TradePlaces(index + 1, index);
                    RecalcRanks();
                }
            }
        }

        private void TradePlaces(int i, int j)
        {
            Cursor.Current = Cursors.WaitCursor;

            Program.manager.TradePlaces(i, j);

            modList.Items.Clear();
            var e = Program.manager.GetEnumerator();
            while (e.MoveNext())
                modList.Items.Add(e.Current.GetListItem());

            modList.SelectedItems.Clear();
            modList.Items[i].Selected = modList.Items[i].Focused = true;
            modList.Focus();

            RecalcRanks();
            applyChangesButton.Enabled = Program.manager.hasChanges;

            Cursor.Current = Cursors.Default;
        }

        public void RecalcRanks()
        {
            for (int i = 0; i < modList.Items.Count; i++)
            {
                var item = modList.Items[i];
                item.SubItems[0] = new ListViewItem.ListViewSubItem(item, $"{i + 1}")
                {
                    BackColor = item.BackColor
                };
            }
            Program.manager.UpdateRanks();
        }

        private void removeModButton_Click(object sender, EventArgs e)
        {
            if(modList.SelectedItems.Count > 0 && MessageBox.Show("This will remove the mod(s) from the list. This action is permanent. Proceed? This might take a while.", "Are you sure?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (Program.settings.SafeMode)
                {
                    Process[] pname = Process.GetProcessesByName(FFXIIPNAME);
                    if (pname.Length > 0)
                    {
                        MessageBox.Show("Final Fantasy XII: The Zodiac Age appears to be running." +
                            " Please close the game before modifying the VBF.");
                        return;
                    }
                }

                Cursor.Current = Cursors.WaitCursor;
                _popup.ShowPopup(Location.X, Location.Y, Width, Height);

                for (int i = 0; i < modList.SelectedItems.Count; i++)
                {
                    Program.manager.RemoveMod(modList.SelectedItems[i].Index, out Mod removedMod, out Mod[] modsBelow, out Mod[] modsAbove);
                    modList.Items.Remove(modList.SelectedItems[i]);
                    Program.manager.ApplyChanges(this, removedMod, modsBelow, modsAbove);
                }

                _popup.ClosePopup();

                if (modList.Items.Count > 0)
                    modList.Items[0].Selected = true;
                else
                {
                    modNameText.Text = "";
                    toolTip1.SetToolTip(modNameText, "No mod selected.");
                    modDescriptionText.Text = "";
                    modAuthorText.Text = "";
                    toolTip1.SetToolTip(modAuthorText, "No mod selected.");
                    modVersionLabel.Text = "";
                    toolTip1.SetToolTip(modVersionLabel, "No mod selected.");
                }
                UpdateStatusLabel();
                RecalcRanks();
                Cursor.Current = Cursors.Default;
            }
        }

        private void exitPicButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toggleModButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (modList.SelectedItems.Count > 0)
            {
                for (int i = 0; i < modList.SelectedItems.Count; i++)
                {
                    var mod = Program.manager.GetMod(modList.SelectedItems[i].Index);
                    if (mod.state == ModState.ACTIVE)
                    {
                        if (modList.SelectedItems.Count == 1)
                            toggleModButton.Image = Properties.Resources.add;
                        mod.state = ModState.INACTIVE;
                        modList.SelectedItems[i].BackColor = _dark ? Program.DarkSubForm : Color.White;
                        modList.SelectedItems[i].SubItems[2].Text = "";
                        applyChangesButton.Enabled = Program.manager.hasChanges = true;
                    }
                    else if (mod.state == ModState.INACTIVE)
                    {
                        if(modList.SelectedItems.Count == 1)
                            toggleModButton.Image = Properties.Resources.disable;
                        mod.state = ModState.ACTIVE;
                        modList.SelectedItems[i].BackColor = _dark ? Color.DarkGreen : Color.LightGreen;
                        modList.SelectedItems[i].SubItems[2].Text = "✓";
                        applyChangesButton.Enabled = Program.manager.hasChanges = true;
                    }
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void updateModButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (modList.SelectedItems.Count == 1)
            {
                if (addModDialog.ShowDialog() ?? false)
                {
                    if (MessageBox.Show("Please ensure that you have closed any files or folders within the mod you selected. Failure to do so might cause the process to fail, and compromise integrity.", "Warning", MessageBoxButtons.YesNo) != DialogResult.Yes)
                        return;

                    var mod = Program.manager.GetMod(modList.SelectedItems[0].Index);
                    var updooted = Program.manager.UpdateMod(addModDialog.SelectedPath, mod, this);

                    if (updooted == null)
                    {
                        MessageBox.Show("The mod is already up to date.");
                        return;
                    }

                    modDescriptionText.Text = updooted.description;
                    modAuthorText.Text = $"By {updooted.author}";
                    modNameText.Text = updooted.modName;
                    toolTip1.SetToolTip(modNameText, updooted.modName);
                    toolTip1.SetToolTip(modAuthorText, updooted.author);
                    toolTip1.SetToolTip(modVersionLabel, updooted.version);
                    modVersionLabel.Text = $"Version: {updooted.version}";
                    modList.Items[modList.SelectedIndices[0]] = updooted.GetListItem();
                    MessageBox.Show("Mod updated!");
                }
            }
           Cursor.Current = Cursors.Default;
        }

        private void applyChangesButton_Click(object sender, EventArgs e)
        {
            if(Program.settings.SafeMode)
            {
                Process[] pname = Process.GetProcessesByName(FFXIIPNAME);
                if (pname.Length > 0)
                {
                    MessageBox.Show("Final Fantasy XII: The Zodiac Age appears to be running." +
                        " Please close the game before modifying the VBF.");
                    return;
                }
            }

            var mods = Program.manager.GetYellowMods();
            if (mods.Count > 0)
            {
                string s = "";
                foreach (var m in mods)
                    s += $"\n{m.modName}";
                if (MessageBox.Show("Some active mods will not install all their files due to being overwritten. Proceed?" + s, "Warning", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    return;
            }

            if(MessageBox.Show("This will extract backups and inject your mods. It might take a while. Proceed?", "Are you sure?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _popup.ShowPopup(Location.X, Location.Y, Width, Height);
                Program.manager.ApplyChanges(this);
                _popup.ClosePopup();
                if (Program.settings.openLog)
                {
                    new LogForm(Program.injectionLog)
                    {
                        Owner = this
                    }.ShowDialog();
                }
                applyChangesButton.Enabled = Program.manager.hasChanges;
                UpdateStatusLabel();
            }
        }

        private void settingsButton_Click(object sender, EventArgs e)
        {
            using (var form = new SettingsForm(this))
            {
                form.Owner = this;
                form.StartPosition = FormStartPosition.CenterParent;
                form.ShowDialog();
                if (form.restoreVanilla)
                {
                    if (!Directory.Exists(Program.manager.vanilla.VANILLAFILESFOLDER)
                        && !Directory.EnumerateFileSystemEntries(Program.manager.vanilla.VANILLAFILESFOLDER, "*", SearchOption.AllDirectories).Any())
                    {
                        MessageBox.Show("There is nothing to restore.");
                        return;
                    }
                    _popup.ShowPopup(Location.X, Location.Y, Width, Height);
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        Program.manager.RestoreVanilla(this);
                        _popup.Message("Reloading");
                        Program.manager.Save();
                        LoadMods();
                        Program.reader.UpdateAfterInjection();
                        Cursor.Current = Cursors.Default;
                        _popup.ClosePopup();
                        MessageBox.Show("Done.");
                        UpdateStatusLabel();
                    }
                    catch (Exception ex)
                    {
                        _popup.ClosePopup();
                        MessageBox.Show("Something went wrong\n" + ex.ToString());
                    }
                }
            };
        }

        public void Log(string message)
        {
            logLabel.Text = message;
            Program.Log(message);
        }

        private void Lab_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(Program.manager != null && Program.manager.hasChanges)
            {
                bool done = false;
                while (!done)
                {
                    if (MessageBox.Show("You have unsaved changes. Do you wish to apply the current active mods?", "Save", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (Program.settings.SafeMode)
                        {
                            Process[] pname = Process.GetProcessesByName(FFXIIPNAME);
                            if (pname.Length > 0)
                            {
                                MessageBox.Show("Final Fantasy XII: The Zodiac Age appears to be running." +
                                    " Please close the game before modifying the VBF.");
                                continue;
                            }
                        }

                        _popup.ShowPopup(Location.X, Location.Y, Width, Height);
                        Program.manager.ApplyChanges(this);
                        Program.manager.AcknowledgeMods();
                        _popup.ClosePopup();
                        done = true;
                    }
                    else
                    {
                        Program.manager.AcknowledgeMods(true);
                        done = true;
                    }
                }
            }
        }

        private void logLabel_Click(object sender, EventArgs e)
        {
            button1_Click(sender, e);
        }

        //open log
        private void button1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            new LogForm(Program.consoleLog)
            {
                Owner = this
            }.ShowDialog();
        }

        private void listFilesButton_Click(object sender, EventArgs e)
        {
            if (modList.SelectedItems.Count == 1)
            {
                var selectedMod = modList.SelectedItems[0].Tag as Mod;
                if (selectedMod.state == ModState.ACTIVE)
                {
                    var scaffold = Program.manager.GetScaffold();
                    var form = new FileListForm(selectedMod, scaffold)
                    {
                        Owner = this,
                        StartPosition = FormStartPosition.CenterParent
                    }.ShowDialog();
                }
                else if (selectedMod.state == ModState.INACTIVE)
                {
                    var form = new FileListForm(selectedMod)
                    {
                        Owner = this,
                        StartPosition = FormStartPosition.CenterParent
                    }.ShowDialog();
                }
            }

        }

        private void aboutBtn_Click(object sender, EventArgs e)
        {
            new AboutForm()
            {
                Owner = this,
                StartPosition = FormStartPosition.CenterParent
            }.ShowDialog();
        }

        private void helpBtn_Click(object sender, EventArgs e)
        {
            new HelpForm()
            {
                Owner = this,
                StartPosition = FormStartPosition.CenterParent
            }.ShowDialog();
        }

        private void toggleDarkMode_Click(object sender, EventArgs e)
        {
            Program.isDarkMode = !Program.isDarkMode;
            Recolor();
        }

        private void mouseDownOnButton(object sender, MouseEventArgs e)
        {
            (sender as PictureBox).SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void mouseUpOnButton(object sender, MouseEventArgs e)
        {
            (sender as PictureBox).SizeMode = PictureBoxSizeMode.Normal;
        }
    }
}
