﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public partial class PopupForm : Form
    {

        private const int CP_NOCLOSE_BUTTON = 0x200;

        private int nextStep;
        private int maxDivided;
        private bool firstTime;

        public PopupForm()
        {
            Initialize();
            firstTime = true;
            var x = Lab.instance.Location.X + (Lab.instance.Size.Width - Width) / 2;
            var y = Lab.instance.Location.Y + (Lab.instance.Size.Height - Height) / 2;
            Location = new Point(Math.Max(x, 0), Math.Max(y, 0));
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        private void Initialize()
        {
            InitializeComponent();

            BackColor = Program.isDarkMode ? Program.DarkTextBG : SystemColors.Control;
            label1.ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.ControlText;
        }

        public void ShowPopup(int ownX, int ownY, int ownW, int ownH)
        {
            if (IsDisposed)
                Initialize();

            var x = ownX + (ownW - Width) / 2;
            var y = ownY + (ownH - Height) / 2;
            Location = new Point(Math.Max(x, 0), Math.Max(y, 0));
            Show();
            BringToFront();
            
        }

        public void Message(string msg)
        {
            label1.Text = msg;
        }

        public void ClosePopup()
        {
            if (!IsDisposed)
            {
                Message("");
                EndProgress();
                Hide();
            }
        }

        public void StartProgress(int max)
        {
            if (IsDisposed)
                Initialize();

            progressBar.Visible = true;
            progressBar.Value = 0;
            progressBar.Step = 1;
            progressBar.Maximum = 100;
            maxDivided = max / 100;
            nextStep = maxDivided;
        }

        public void StepProgress(int step)
        {
            if (step + 1 > nextStep)
            {
                nextStep += maxDivided;
                progressBar.PerformStep();
            }
        }

        public void EndProgress()
        {
            if (IsDisposed)
                Initialize();
            progressBar.Visible = false;
        }

        private void PopupForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }

        private void PopupForm_Shown(object sender, EventArgs e)
        {
            if (firstTime)
            {
                firstTime = false;
                Hide();
            }
        }
    }
}
