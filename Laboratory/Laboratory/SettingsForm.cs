﻿using Ookii.Dialogs.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public partial class SettingsForm : Form
    {

        private bool settingUp = false;
        private VistaFolderBrowserDialog dialog;
        private Lab lab;
        public bool restoreVanilla { get; private set; }

        public SettingsForm(Lab lab)
        {
            InitializeComponent();
            showReport.Checked = Program.settings.openLog;

            if (Program.settings.useCustomTemp)
            {
                settingUp = true;
                useCustomTemp.Checked = alterTemp.Enabled = Program.settings.useCustomTemp;
                folderPath.Text = Program.settings.tempPath;
            }

            enableLogging.Checked = Program.settings.saveLog;
            startupCB.Checked = Program.settings.Startup;

            BackColor = Program.isDarkMode ? Program.DarkForm : SystemColors.Control;
            showReport.ForeColor =
                useCustomTemp.ForeColor =
                enableLogging.ForeColor =
                startupCB.ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText;

            folderPath.ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText;
            folderPath.BackColor = Program.isDarkMode ? Program.DarkSubForm : SystemColors.Window;

            dialog = new VistaFolderBrowserDialog()
            {
                ShowNewFolderButton = true
            };

            this.lab = lab;
            restoreVanilla = false;

            try
            {
                restoreButton.Visible =
                    restoreButton.Enabled = !(Program.fileSystem is null)
                    && Directory.Exists(Program.manager.vanilla.VANILLAFILESFOLDER)
                    && Directory.EnumerateFileSystemEntries(Program.manager.vanilla.VANILLAFILESFOLDER, "*", SearchOption.AllDirectories).Any();
            }
            catch(Exception)
            {
                restoreButton.Visible = false;
            }

        }

        private void showReport_CheckedChanged(object sender, EventArgs e)
        {
            Program.settings.openLog = showReport.Checked;
        }

        private void useCustomTemp_CheckedChanged(object sender, EventArgs e)
        {
            if (settingUp)
            {
                settingUp = false;
                return;
            }

            if (useCustomTemp.Checked && folderPath.Text.Equals(String.Empty))
            {
                if (dialog.ShowDialog() ?? false)
                {
                    var temp = dialog.SelectedPath;

                    var di = new DirectoryInfo(temp);
                    if(!di.HasFullAccess())
                    {
                        useCustomTemp.Checked = false;
                        MessageBox.Show("You do not have full permissions upon this folder." +
                            " Please choose another one.");
                        return;
                    }

                    folderPath.Text = Program.settings.tempPath = temp;
                }
                else
                {
                    useCustomTemp.Checked = false;
                    folderPath.Text = "";
                }
            }
            else
                folderPath.Text = "";
            Program.settings.useCustomTemp = alterTemp.Enabled = useCustomTemp.Checked;
        }

        private void alterTemp_Click(object sender, EventArgs e)
        {
            if (dialog.ShowDialog() ?? false)
            {
                folderPath.Text = Program.settings.tempPath = dialog.SelectedPath;
            }
        }

        private void enableLogging_CheckedChanged(object sender, EventArgs e)
        {
            Program.settings.saveLog = enableLogging.Checked;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingsForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == (Keys.Shift | Keys.Control) && e.KeyCode == Keys.S)
            {
                Program.settings.SafeMode = !Program.settings.SafeMode;
                MessageBox.Show("Safe mode has been " + (Program.settings.SafeMode ? "enabled!" : "disabled."));
            }
        }

        private void startupCB_CheckedChanged(object sender, EventArgs e)
        {
            Program.settings.Startup = startupCB.Checked;
        }

        private void restoreButton_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("All your mods will be disabled and your game will be restored to its original form. Proceed?", "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                restoreVanilla = true;
                Close();
            }
        }
    }
}
