﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    class VanillaTracker
    {
        public string VANILLAFILESFOLDER = $"{Program.reader.mBigFileFolder + "\\" + Program.fileSystem.systemName}_mods\\_vanilla\\";

        public VanillaTracker()
        {
            try
            {
                if (!Directory.Exists(VANILLAFILESFOLDER))
                    Directory.CreateDirectory(VANILLAFILESFOLDER);
            }
            catch(Exception e)
            {
                MessageBox.Show("Failed to create vanilla files folder.");
                throw e;
            }
        }

        public void Extract(ModFile[] scaffold, string tempFolder, out int backed, out int reverted)
        {
            Lab._popup.Message("Making backups");
            var currentFiles = ModManager.GetAllFiles(VANILLAFILESFOLDER);
            var virtualFiles = new List<string>();

            foreach (var file in currentFiles)
                virtualFiles.Add(file.Replace(VANILLAFILESFOLDER, "").Replace('\\', '/'));

            var toExtract = new List<string>();
            var toKeep = new List<string>();

            foreach (var modFile in scaffold)
            {
                if (virtualFiles.Contains(modFile.virtualPath))
                    toKeep.Add(modFile.virtualPath);
                else
                    toExtract.Add(modFile.virtualPath);
            }

            var toDelete = virtualFiles.Except(toKeep);

            if (toDelete.Count() > 0)
            {
                Lab._popup.Message("Deleting files");
                Lab._popup.StartProgress(toDelete.Count());
                int i = 0;
                foreach(var toDel in toDelete)
                {
                    var filePath = VANILLAFILESFOLDER + "\\" + toDel.Replace('/', '\\');
                    var newPath = tempFolder + "\\" + toDel.Replace('/', '\\');
                    var folders = Path.GetDirectoryName(newPath);
                    Directory.CreateDirectory(folders);
                    File.Copy(filePath, newPath);
                    File.Delete(filePath);
                    Lab._popup.StepProgress(i++);
                }
                Lab._popup.EndProgress();
            }

            reverted = toDelete.Count();
            backed = 0;
            Lab._popup.Message("Extracting files");
            Lab._popup.StartProgress(toExtract.Count());
            foreach (var toEx in toExtract)
            {
                var path = VANILLAFILESFOLDER + toEx.Replace('/', '\\');

                var dir = Path.GetDirectoryName(path);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                Program.reader.ExtractFileContents(toEx, path);
                Lab._popup.StepProgress(backed++);
            }
            Lab._popup.EndProgress();

        }

        public void Extract(string[] scaffold, string tempFolder, out int backed, out int reverted)
        {
            Lab._popup.Message("Making backups");
            var currentFiles = ModManager.GetAllFiles(VANILLAFILESFOLDER);
            var virtualFiles = new List<string>();

            foreach (var file in currentFiles)
                virtualFiles.Add(file.Replace(VANILLAFILESFOLDER, "").Replace('\\', '/'));

            var toExtract = new List<string>();
            var toKeep = new List<string>();

            foreach (var virtualFile in scaffold)
            {
                if (virtualFiles.Contains(virtualFile))
                    toKeep.Add(virtualFile);
                else
                    toExtract.Add(virtualFile);
            }

            var toDelete = virtualFiles.Except(toKeep);

            if (toDelete.Count() > 0)
            {
                Lab._popup.Message("Deleting files");
                Lab._popup.StartProgress(toDelete.Count());
                int i = 0;
                foreach (var toDel in toDelete)
                {
                    var filePath = VANILLAFILESFOLDER + "\\" + toDel.Replace('/', '\\');
                    var newPath = tempFolder + "\\" + toDel.Replace('/', '\\');
                    var folders = Path.GetDirectoryName(newPath);
                    Directory.CreateDirectory(folders);
                    File.Copy(filePath, newPath);
                    File.Delete(filePath);
                    Lab._popup.StepProgress(i++);
                }
                Lab._popup.EndProgress();
            }

            reverted = toDelete.Count();
            backed = 0;
            Lab._popup.Message("Extracting files");
            Lab._popup.StartProgress(toExtract.Count());
            foreach (var toEx in toExtract)
            {
                var path = VANILLAFILESFOLDER + toEx.Replace('/', '\\');

                var dir = Path.GetDirectoryName(path);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                Program.reader.ExtractFileContents(toEx, path);
                Lab._popup.StepProgress(backed++);
            }
            Lab._popup.EndProgress();

        }

    }
}
