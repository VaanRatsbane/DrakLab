﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using Ookii.Dialogs.Wpf;

namespace Laboratory
{
    class ModManager
    {
        string MODFOLDER = $"\\{Program.fileSystem.systemName}_mods\\";
        string MANAGERSETTINGS = $"\\{Program.fileSystem.systemName}_mods\\modsettings.json";
        string LOADEDMODS = $"\\{Program.fileSystem.systemName}_mods\\loaded.json";

        public VanillaTracker vanilla;
        public int modCount { get { return mods.Count; } }
        public int activeCount { get { return mods.Count(mod => mod.state == ModState.ACTIVE); } }

        public List<Mod> mods;

        ModFile[] currentScaffold;

        public bool hasChanges;

        public ModManager()
        {
            try
            {
                if (!Directory.Exists(Program.reader.mBigFileFolder + MODFOLDER))
                    Directory.CreateDirectory(Program.reader.mBigFileFolder + MODFOLDER);                    

                var json = File.ReadAllText(Program.reader.mBigFileFolder + LOADEDMODS);
                mods = JsonConvert.DeserializeObject<List<Mod>>(json);
            }
            catch(Exception)
            {
                mods = new List<Mod>();
            }            

            vanilla = new VanillaTracker();
            hasChanges = false;
        }

        public void Save()
        {
            try
            {
                var json = JsonConvert.SerializeObject(mods, Formatting.Indented);
                File.WriteAllText(Program.reader.mBigFileFolder + LOADEDMODS, json);
            }
            catch(Exception e)
            {
                MessageBox.Show($"Couldn't save mod settings!\n{e.ToString()}");
            }
        }

        public void AcknowledgeMods(bool setInactive = false)
        {
            foreach (var mod in mods)
            {
                if (setInactive && mod.newMod)
                        mod.state = ModState.INACTIVE;
                mod.newMod = false;
            }
        }

        public void ApplyChanges(Lab lab, Mod removedMod = null, Mod[] modsBelow = null, Mod[] modsAbove = null)
        {
            Cursor.Current = Cursors.WaitCursor;
            string tempDirectory = "";

            try
            {
                if (!hasChanges)
                {
                    return;
                }

                Save();

                var usingCustomPath = false;
                if (Program.settings.useCustomTemp && !(Program.settings.tempPath is null))
                {
                    if (Directory.Exists(Program.settings.tempPath))
                    {
                        usingCustomPath = true;
                    }
                    else
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("Cannot use the given custom temp path. Using the system's default...");
                        Cursor.Current = Cursors.WaitCursor;
                    }
                }

                var tempFolder = usingCustomPath ? Program.settings.tempPath : Path.GetTempPath();
                {
                    DirectoryInfo d = null;
                    do
                    {
                        if(d is null)
                            d = new DirectoryInfo(tempFolder);
                        else
                        {
                            var result = MessageBox.Show("You don't have full permissions over the temporary folder. " +
                                "Do you wish to choose a new one? (Choosing 'No' will use the software's own folder.", "Access Denied", MessageBoxButtons.YesNo);
                            if(result == DialogResult.Yes)
                            {
                                var dialog = new VistaFolderBrowserDialog();
                                if(dialog.ShowDialog() ?? false)
                                    tempFolder = dialog.SelectedPath;
                                else
                                    tempFolder = Directory.GetCurrentDirectory();
                            }
                            else
                                tempFolder = Directory.GetCurrentDirectory();
                        }
                        d = new DirectoryInfo(tempFolder);
                    }
                    while (!d.HasFullAccess());
                }

                do
                {
                    tempDirectory = Path.Combine(tempFolder, "DRAKLAB" + Path.GetRandomFileName());
                } while (Directory.Exists(tempDirectory));
                
                lab.Log("Calculating files...");
                var scaffold = Scaffold.GetScaffold(mods);
                lab.Log("Making backups...");
                vanilla.Extract(scaffold, tempDirectory, out int backed, out int reverted);
                lab.Log($"Backed up {backed} files.");

                if (removedMod != null) //make sure to only inject previously overwritten files
                {
                    var oldScaffoldOfMod = new List<string>();
                    foreach(var oldFile in removedMod.files)
                    {
                        if (!modsAbove.Any(c => c.files.Any(d => d.virtualPath == oldFile.virtualPath)))
                            oldScaffoldOfMod.Add(oldFile.virtualPath);
                    }
                    scaffold = Scaffold.GetScaffold(modsBelow.ToList())
                        .Where(c => oldScaffoldOfMod.Any(vpath => vpath == c.virtualPath)).ToArray();
                }
                else
                    scaffold = scaffold.Where(c => c.parent.originalState == ModState.INACTIVE).ToArray(); //No point injecting files that were already injected

                if (scaffold.Length > 0 || reverted > 0)
                {
                    lab.Log("Preparing injection...");

                    Lab._popup.Message("Preparing injection");
                    int injectedFromScaffold = 0;
                    int i = 0;
                    Lab._popup.StartProgress(scaffold.Count());
                    foreach (var modFile in scaffold)
                    {
                        if (!currentScaffold.Contains(modFile))
                        {
                            injectedFromScaffold++;
                            modFile.parent.newMod = false;
                            var newPath = tempDirectory + '\\' + modFile.virtualPath.Replace('/', '\\');
                            var folders = Path.GetDirectoryName(newPath);
                            Directory.CreateDirectory(folders);
                            File.Copy(modFile.filePath, newPath);
                        }
                        Lab._popup.StepProgress(i++);
                    }
                    Lab._popup.EndProgress();

                    lab.Log("Initializing ff12-vbf.exe...");

                    var logList = new List<string>();

                    Process process = new Process();
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.FileName = "ff12-vbf\\ff12-vbf.exe";
                    process.StartInfo.Arguments = $"-r \"{tempDirectory}\" \"{Program.reader.mBigFilePath}\"";

                    process.OutputDataReceived += (sender, args) => logList.Add(args.Data);
                    process.ErrorDataReceived += (sender, args) => logList.Add(args.Data);

                    Lab._popup.Message("Injecting files, please wait...");

                    process.Start();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();
                    process.WaitForExit();

                    Program.InjectionLog(logList);

                    lab.Log($"Injected {injectedFromScaffold} files and reverted {reverted} original files.");

                    Save();
                    hasChanges = false;
                    currentScaffold = scaffold;
                    foreach (var mod in mods)
                        mod.UpdateOriginalState();

                    Lab._popup.ClosePopup();

                    MessageBox.Show("Done. Reloading VBF...");

                    Lab._popup.ShowPopup(lab.Location.X, lab.Location.Y, lab.Width, lab.Height);
                    Lab._popup.Message("Reloading");

                    Program.reader.UpdateAfterInjection();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            finally
            {
                if(Directory.Exists(tempDirectory))
                    DeleteFilesAndFoldersRecursively(tempDirectory);
            }

            Cursor.Current = Cursors.Default;
        }

        public void RestoreVanilla(Lab lab)
        {
            Lab._popup.Message("Restoring files");
            lab.Log("Initializing ff12-vbf.exe...");

            var logList = new List<string>();

            Process process = new Process();
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = "ff12-vbf\\ff12-vbf.exe";
            process.StartInfo.Arguments = $"-r \"{vanilla.VANILLAFILESFOLDER.TrimEnd('\\')}\" \"{Program.reader.mBigFilePath}\"";

            process.OutputDataReceived += (sender, args) => logList.Add(args.Data);
            process.ErrorDataReceived += (sender, args) => logList.Add(args.Data);

            Lab._popup.Message("Injecting files, please wait...");

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();

            Lab._popup.Message("Deleting backups");
            var di = new DirectoryInfo(vanilla.VANILLAFILESFOLDER);
            foreach (var dir in di.EnumerateDirectories())
                dir.Delete(true);
            foreach (var file in di.EnumerateFiles())
                file.Delete();

            foreach(var mod in mods)
                mod.Disable();
            
        }

        public Mod AddMod(string sourceFolder, Lab lab)
        {
            Lab._popup.Message("Setting up");
            var fullModPath = Program.reader.mBigFileFolder + MODFOLDER;
            var frags = sourceFolder.Split('\\');
            var prefix = String.Join("\\", frags, 0, frags.Length - 1) + "\\" + frags.Last();
            var modFolder = fullModPath + frags.Last();

            if(mods.Count(o => o.modFolder == modFolder) == 1 && Directory.Exists(modFolder))
            {
                MessageBox.Show("There already exists a directory with that name in the mods folder.");
                return null;
            }

            var subdirectoryEntries = Directory.GetDirectories(sourceFolder);
            var files = new List<string>();
            foreach(var folder in subdirectoryEntries)
            {
                files.AddRange(GetAllFiles(folder));
            }
            bool isValidInjection = Program.fileSystem.VerifyInjection(prefix, files, out string[] report);

            if(report != null && report.Length > 0)
            {
                foreach (var rep in report)
                    lab.Log(rep);
            }

            if(isValidInjection)
            {
                Lab._popup.Message("Copying mod folder");

                if (!prefix.Contains(fullModPath))
                    DirectoryCopy(sourceFolder, modFolder, true);

                string author = "";
                string title = frags.Last();
                string version = "";
                string description = "";

                try
                {
                    if (File.Exists(modFolder + "\\mod.ini"))
                    {
                        var lines = File.ReadAllLines(modFolder + "\\mod.ini");
                        foreach (var line in lines)
                        {
                            if (line.StartsWith("#")) continue;

                            var ignoredHash = line.Split('#');
                            var parts = ignoredHash[0].Split('=');
                            switch (parts[0].ToLower().TrimEnd(' '))
                            {
                                case "author":
                                    author = parts[1].TrimStart(' ');
                                    break;

                                case "title":
                                    title = parts[1].TrimStart(' ');
                                    break;

                                case "version":
                                    version = parts[1].TrimStart(' ');
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show("Error reading mod.ini\nNot all metadata will be displayed.");
                }

                try
                {
                    if(File.Exists(modFolder + "\\readme.txt"))
                    {
                        description = File.ReadAllText(modFolder + "\\readme.txt");
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show("Error reading mod details. Description will be left blank.");
                }

                Lab._popup.Message("Finishing");

                var mod = new Mod(modFolder, title, description, author, version, mods.Count + 1);
                mods.Add(mod);
                lab.Log($"Mod {title} loaded.");
                hasChanges = true;

                return mod;
            }
            else
            {
                MessageBox.Show("Error loading the mod. Check the log.");
                return null;
            }
        }

        public void RemoveMod(int pos, out Mod mod, out Mod[] modsBelow, out Mod[] modsAbove)
        {
            Lab._popup.Message("Deleting mod folder");
            mod = mods[pos];
            if (mod.originalState != ModState.ACTIVE && mods.Count > pos + 1) //no point scaffolding if the mod wasnt even injected to begin with
            {
                modsBelow = mods.GetRange(pos + 1, mods.Count - (pos + 1)).Where(c => c.originalState == ModState.ACTIVE).ToArray();
                modsAbove = mods.GetRange(0, pos).Where(c => c.originalState == ModState.ACTIVE).ToArray();
            }
            else
            {
                modsBelow = null;
                modsAbove = null;
            }
            DisableModState(pos);
            Directory.Delete(mod.modFolder, true);
            mods.RemoveAt(pos);
            if(!hasChanges)
                Save();
        }

        public Mod UpdateMod(string newFolder, Mod mod, Lab lab)
        {
            Lab._popup.Message("Setting up");

            if (mod.state == ModState.ACTIVE)
                UpdateCurrentScaffold();

            var fullModPath = Program.reader.mBigFileFolder + MODFOLDER;
            var frags = newFolder.Split('\\');
            var prefix = String.Join("\\", frags, 0, frags.Length - 1) + '\\' + frags.Last();
            var modFolder = fullModPath + frags.Last();

            var subdirectoryEntries = Directory.GetDirectories(newFolder);
            var files = new List<string>();
            foreach (var folder in subdirectoryEntries)
            {
                files.AddRange(GetAllFiles(folder));
            }

            bool isValidInjection = Program.fileSystem.VerifyInjection(prefix, files, out string[] report);

            prefix = prefix.Replace('\\', '/') + '/';
            files = files.Select(c => c.Replace('\\', '/')).ToList(); //sanitize

            if (report != null && report.Length > 0)
            {
                foreach (var rep in report)
                    lab.Log(rep);
            }

            if (isValidInjection)
            {
                Lab._popup.Message("Copying files");

                fullModPath = fullModPath.Replace('\\', '/');

                var originalVirtuals = mod.files.Select(c => c.virtualPath);
                var newVirtuals = files.Select(c => c.Replace(prefix, ""));
                var otherMods = mods.OrderBy(c => c.rank).Where(c => c.state == ModState.ACTIVE && c != mod);
                var modsAbove = otherMods.Where(c => c.rank < mod.rank);

                var filesToInject = new List<string>(); //physical paths
                var filesToBackup = new List<string>(); //virtual paths
                var projectedScaffold = currentScaffold.Select(c => c.virtualPath).ToList(); //virtual paths

                //Handle new files
                var dirsToCreate = new List<string>();
                var filesToCopy = new List<Tuple<string, string>>();
                foreach (var file in newVirtuals.Where(c => !originalVirtuals.Contains(c)))
                {
                    if (mod.state == ModState.ACTIVE)
                    {
                        if (!otherMods.Any(c => c.files.Any(d => d.virtualPath == file))) //if no other mods have the file, extract to vanilla
                            projectedScaffold.Add(file);
                        if (!modsAbove.Any(c => c.files.Any(d => d.virtualPath == file))) //if active mods above contain file, do not inject
                             filesToInject.Add(prefix + file);
                    }
                    if (!prefix.Contains(fullModPath)) //if new mod is not in modfolder, copy files and create necessary folders
                    {
                        var newPath = (mod.modFolder + file).Replace('\\', '/');
                        dirsToCreate.Add(newPath);
                        filesToCopy.Add(new Tuple<string, string>(prefix + file, mod.modFolder + file)); //copy new file to storage mod folder
                    }
                }

                //Handle updated files
                foreach (var file in newVirtuals.Where(c => originalVirtuals.Contains(c)))
                {
                    var modFile = mod.files.First(c => c.virtualPath == file);                        
                    var newInfo = new FileInfo(prefix + file);
                    var oldInfo = new FileInfo(mod.modFolder + '\\' + file);
                    if(newInfo.LastWriteTime > oldInfo.LastWriteTime) //if new file is more recent, updoot
                    {
                        if (!prefix.StartsWith(fullModPath)) //if new mod is not in modfolder, overwrite old files
                        {
                            filesToCopy.Add(new Tuple<string, string>(prefix + file, modFile.filePath));
                        }
                        if (mod.state == ModState.ACTIVE && currentScaffold.Contains(modFile)) //if mod is active and file is in scaffold, inject
                            filesToInject.Add(prefix + file);
                    }
                }

                //Handle old files (to be removed)
                var filesToDelete = new List<string>();
                foreach (var file in originalVirtuals.Where(c => !newVirtuals.Contains(c)))
                {
                    var modFile = mod.files.First(c => c.virtualPath == file);
                    if (currentScaffold.Contains(modFile)) //if current mod had this file in scaffold, inject
                    {
                        var newScaffoldedFile = otherMods.FirstOrDefault(c => c.files.Any(d => d.virtualPath == modFile.virtualPath))?.files?.FirstOrDefault(c => c.virtualPath == modFile.virtualPath);
                        if(newScaffoldedFile is null) //if no other active mod has this file
                            projectedScaffold.Remove(file); //inject vanilla
                        else
                            filesToInject.Add(newScaffoldedFile.filePath); //inject the other mod
                    }

                    if (!prefix.Contains(fullModPath)) //if new mod is not in modfolder, delete old files
                    {
                        filesToDelete.Add(mod.modFolder + "\\" + file);
                    }
                }

                //There's nothing to update
                if (filesToCopy.Count == 0 && filesToDelete.Count == 0)
                    return null;

                foreach(var path in dirsToCreate)
                    Directory.CreateDirectory(Path.GetDirectoryName(path));
                foreach(var file in filesToCopy)
                    File.Copy(file.Item1, file.Item2, true); //overwrite old file
                foreach (var file in filesToDelete)
                    File.Delete(file);

                if (prefix.Contains(fullModPath)) //if new mod is already in modfolder, can safely delete it
                {
                    DeleteFilesAndFoldersRecursively(mod.modFolder);
                }
                else //rename old folder, since all needed files have been copied, and old ones deleted
                {
                    if(mod.modFolder.Replace('\\', '/') != modFolder.Replace('\\', '/')) //only if it's not the same name!
                        Directory.Move(mod.modFolder, modFolder);
                }

                Lab._popup.Message("Handling metadata...");

                string author = "";
                string title = frags.Last();
                string version = "";
                string description = "";

                try
                {
                    if (File.Exists(modFolder + "\\mod.ini"))
                    {
                        var lines = File.ReadAllLines(modFolder + "\\mod.ini");
                        foreach (var line in lines)
                        {
                            if (line.StartsWith("#")) continue;

                            var ignoredHash = line.Split('#');
                            var parts = ignoredHash[0].Split('=');
                            switch (parts[0].ToLower().TrimEnd(' '))
                            {
                                case "author":
                                    author = parts[1].TrimStart(' ');
                                    break;

                                case "title":
                                    title = parts[1].TrimStart(' ');
                                    break;

                                case "version":
                                    version = parts[1].TrimStart(' ');
                                    break;

                                default:
                                    break;
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Error reading mod.ini\nNot all metadata will be displayed.");
                }

                try
                {
                    if (File.Exists(modFolder + "\\readme.txt"))
                    {
                        description = File.ReadAllText(modFolder + "\\readme.txt");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Error reading mod details. Description will be left blank.");
                }
                
                if (mod.state == ModState.ACTIVE && filesToInject.Count > 0)
                {
                    Lab._popup.Message("Injecting needed files...");
                    Cursor.Current = Cursors.WaitCursor;
                    string tempDirectory = "";

                    try
                    {

                        var usingCustomPath = false;
                        if (Program.settings.useCustomTemp && !(Program.settings.tempPath is null))
                        {
                            if (Directory.Exists(Program.settings.tempPath))
                            {
                                usingCustomPath = true;
                            }
                            else
                            {
                                Cursor.Current = Cursors.Default;
                                MessageBox.Show("Cannot use the given custom temp path. Using the system's default...");
                                Cursor.Current = Cursors.WaitCursor;
                            }
                        }

                        var tempFolder = usingCustomPath ? Program.settings.tempPath : Path.GetTempPath();
                        {
                            DirectoryInfo d = null;
                            do
                            {
                                if (d is null)
                                    d = new DirectoryInfo(tempFolder);
                                else
                                {
                                    var result = MessageBox.Show("You don't have full permissions over the temporary folder. " +
                                        "Do you wish to choose a new one? (Choosing 'No' will use the software's own folder.", "Access Denied", MessageBoxButtons.YesNo);
                                    if (result == DialogResult.Yes)
                                    {
                                        var dialog = new VistaFolderBrowserDialog();
                                        if (dialog.ShowDialog() ?? false)
                                            tempFolder = dialog.SelectedPath;
                                        else
                                            tempFolder = Directory.GetCurrentDirectory();
                                    }
                                    else
                                        tempFolder = Directory.GetCurrentDirectory();
                                }
                                d = new DirectoryInfo(tempFolder);
                            }
                            while (!d.HasFullAccess());
                        }

                        do
                        {
                            tempDirectory = Path.Combine(tempFolder, "DRAKLAB" + Path.GetRandomFileName());
                        } while (Directory.Exists(tempDirectory));

                        lab.Log("Calculating files...");
                        vanilla.Extract(projectedScaffold.ToArray(), tempDirectory, out int backed, out int reverted);
                        lab.Log($"Backed up {backed} files.");

                        lab.Log("Preparing injection...");
                        Lab._popup.Message("Preparing injection");
                        int i = 0;
                        Lab._popup.StartProgress(filesToInject.Count());
                        foreach (var injectable in filesToInject)
                        {
                            var virtualPath = injectable.Replace(prefix, "");
                            var newPath = tempDirectory + '\\' + virtualPath.Replace('/', '\\');
                            var folders = Path.GetDirectoryName(newPath);
                            Directory.CreateDirectory(folders);
                            File.Copy(injectable, newPath);
                            Lab._popup.StepProgress(i++);
                        }
                        Lab._popup.EndProgress();

                        lab.Log("Initializing ff12-vbf.exe...");

                        var logList = new List<string>();

                        Process process = new Process();
                        process.StartInfo.RedirectStandardOutput = true;
                        process.StartInfo.RedirectStandardError = true;
                        process.StartInfo.UseShellExecute = false;
                        process.StartInfo.CreateNoWindow = true;
                        process.StartInfo.FileName = "ff12-vbf\\ff12-vbf.exe";
                        process.StartInfo.Arguments = $"-r \"{tempDirectory}\" \"{Program.reader.mBigFilePath}\"";

                        process.OutputDataReceived += (sender, args) => logList.Add(args.Data);
                        process.ErrorDataReceived += (sender, args) => logList.Add(args.Data);

                        Lab._popup.Message("Injecting files, please wait...");

                        process.Start();
                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();
                        process.WaitForExit();

                        Program.InjectionLog(logList);

                        lab.Log($"Injected {filesToInject.Count()} files and reverted {reverted} original files.");
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.ToString());
                    }
                    finally
                    {
                        if (Directory.Exists(tempDirectory))
                            DeleteFilesAndFoldersRecursively(tempDirectory);
                    }

                    Cursor.Current = Cursors.Default;
                    Lab._popup.ClosePopup();
                    MessageBox.Show("Done. Reloading VBF...");

                    Program.reader.UpdateAfterInjection();
                }


                mod.Reset(modFolder, title, description, author, version);
                UpdateCurrentScaffold();
                Save();

                lab.Log($"Mod {title} updated.");

                return mod;
            }
            else
            {
                MessageBox.Show("Error updating the mod. Check the log.");
                return null;
            }
        }

        public void EnableModState(int pos)
        {
            mods[pos].state = ModState.ACTIVE;
            hasChanges = true;
        }

        public void DisableModState(int pos)
        {
            if (mods[pos].state == ModState.INACTIVE && !hasChanges)
                return;

            mods[pos].state = ModState.INACTIVE;
            hasChanges = true;
        }

        public void DisableAllMods()
        {
            foreach (var mod in mods)
            {
                if (mod.state != ModState.INACTIVE)
                {
                    hasChanges = true;
                    mod.state = ModState.INACTIVE;
                }
            }
        }

        public void UpdateCurrentScaffold()
        {
            currentScaffold = Scaffold.GetScaffold(mods);
        }

        public static List<string> GetAllFiles(string targetDirectory)
        {
            var files = new List<string>();

            // Process the list of files found in the directory.
            files.AddRange(Directory.GetFiles(targetDirectory));

            // Recurse into subdirectories of this directory.
            var subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
                files.AddRange(GetAllFiles(subdirectory));
            return files;
        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public IEnumerator<Mod> GetEnumerator()
        {
            return mods.GetEnumerator();
        }

        public Mod GetMod(int pos)
        {
            return mods[pos];
        }

        public void TradePlaces(int i, int j)
        {
            var mod = mods[i];
            mods[i] = mods[j];
            mods[j] = mod;
            hasChanges = mods[i].state == ModState.ACTIVE && mods[i].state == ModState.ACTIVE;
        }

        public List<Mod> ModsAbove(Mod mod)
        {
            var list = new List<Mod>();
            foreach (var m in mods)
                if (mod != m)
                    list.Add(m);
                else
                    break;
            return list;
        }

        public ModFile[] GetScaffold()
        {
            return Scaffold.GetScaffold(mods);
        }

        public static void DeleteFilesAndFoldersRecursively(string target_dir)
        {
            foreach (string file in Directory.GetFiles(target_dir))
            {
                File.Delete(file);
            }

            foreach (string subDir in Directory.GetDirectories(target_dir))
            {
                DeleteFilesAndFoldersRecursively(subDir);
            }

            Thread.Sleep(1); // This makes the difference between whether it works or not. Sleep(0) is not enough.
            Directory.Delete(target_dir);
        }

        public List<Mod> GetYellowMods()
        {
            var scaffold = GetScaffold();
            var result = new List<Mod>();
            foreach(var mod in mods)
            if (mod.state == ModState.ACTIVE)
            {
                foreach (var file in mod.files)
                {
                    if (!scaffold.Contains(file))
                    {
                        result.Add(mod);
                        break;
                    }
                }
            }
            return result;
        }

        public void UpdateRanks()
        {
            for (int i = 0; i < mods.Count; i++)
                mods[i].rank = i + 1;
        }

    }
}
