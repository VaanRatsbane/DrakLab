﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public partial class FileListForm : Form
    {
        public FileListForm(Mod mod, ModFile[] scaffold)
        {
            InitializeComponent();
            var inScaffold = new List<ModFile>();
            var notInScaffold = new List<ModFile>();
            foreach (var file in mod.files)
            {
                if (scaffold.Contains(file))
                    inScaffold.Add(file);
                else
                    notInScaffold.Add(file);
            }
            foreach (var file in inScaffold)
                listView1.Items.Add(new ListViewItem(file.virtualPath)
                {
                    BackColor = Program.isDarkMode ? Color.DarkGreen : Color.LightGreen,
                    ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText
                });

            foreach (var file in notInScaffold)
                listView1.Items.Add(new ListViewItem(file.virtualPath)
                {
                    BackColor = Program.isDarkMode ? Color.DarkOrange : Color.Orange,
                    ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText
                });

            BackColor = Program.isDarkMode ? Program.DarkForm : SystemColors.Control;
            listView1.BackColor = Program.isDarkMode ? Program.DarkSubForm : SystemColors.Window;

            scaffoldLabel.Text = $"{inScaffold.Count} active file(s)";
            scaffoldLabel.BackColor = Program.isDarkMode ? Color.DarkGreen : Color.LightGreen;
            scaffoldLabel.ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText;

            notScaffoldLabel.Text = $"{notInScaffold.Count} overwritten file(s)";
            notScaffoldLabel.BackColor = Program.isDarkMode ? Color.DarkOrange : Color.Orange;
            notScaffoldLabel.ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText;

        }

        public FileListForm(Mod mod)
        {
            InitializeComponent();
            foreach (var file in mod.files)
            {
                listView1.Items.Add(new ListViewItem(file.virtualPath));
            }

            BackColor = Program.isDarkMode ? Program.DarkForm : SystemColors.Control;
            listView1.BackColor = Program.isDarkMode ? Program.DarkSubForm : SystemColors.Window;
            listView1.ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText;

            scaffoldLabel.Text = $"{mod.files.Count} files";
            scaffoldLabel.BackColor = Program.isDarkMode ? Color.DarkGreen : Color.LightGreen;
            scaffoldLabel.ForeColor = Program.isDarkMode ? SystemColors.Control : SystemColors.WindowText;

            notScaffoldLabel.Text = $"";
            notScaffoldLabel.BackColor = Color.Transparent;

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
