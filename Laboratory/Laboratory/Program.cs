﻿using Laboratory.VBFTool;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    /*
     Changelog
     Added dark mode toggle button.
     Added help menu, with a video guide link.
     Changed location of the settings file to no longer save near the VBF, but rather in the same folder as the software.
     Changed mod addition dialog to Ookii's dialog.
     Software now tries to guess where the VBF you want to load is, and remembers where it was last.
     */
    static class Program
    {

        public static VirtuosBigFileReader reader;
        public static VFileSystem fileSystem;
        public static ModManager manager;
        public static Queue<string> consoleLog;
        public static List<string> injectionLog;

        public static Settings settings;

        public static bool isDarkMode { get { return settings.DarkMode; } set { settings.DarkMode = value; } }
        public static Color DarkForm = Color.FromArgb(47, 49, 54);
        public static Color DarkSubForm = Color.FromArgb(54, 57, 63);
        public static Color DarkTextBG = Color.FromArgb(74, 77, 83);

        const string SETTINGS = "settings.json";
        const int LOGSIZE = 50;
        public static string FFXIIPATH = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\Steam\\steamapps\\common\\FINAL FANTASY XII THE ZODIAC AGE";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            consoleLog = new Queue<string>(LOGSIZE);
            Program.fileSystem = null;
            LoadSettings();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Lab());
            SaveSettings();
            Environment.Exit(0);
        }

        private static void LoadSettings()
        {
            try
            {
                var json = File.ReadAllText(SETTINGS);
                try
                {
                    settings = JsonConvert.DeserializeObject<Settings>(json);
                }
                catch (JsonException)
                {
                    settings = new Settings()
                    {
                        DarkMode = false,
                        openLog = false,
                        saveLog = false,
                        tempPath = "",
                        useCustomTemp = false,
                        LastVBFLocation = "",
                        SafeMode = true,
                        Startup = true
                    };
                }
                if (settings.LastVBFLocation.Equals(String.Empty))
                    settings.LastVBFLocation = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + "\\Steam\\steamapps\\common\\FINAL FANTASY XII THE ZODIAC AGE";

            }
            catch (Exception)
            {
                settings = new Settings();
            }
        }

        private static void SaveSettings()
        {
            try
            {
                var json = JsonConvert.SerializeObject(settings, Formatting.Indented);
                File.WriteAllText(SETTINGS, json);
            }
            catch (Exception e)
            {
                MessageBox.Show($"Couldn't save DrakLab settings!\n{e.ToString()}");
            }
        }

        public static void Log(string message)
        {
            if (consoleLog.Count == LOGSIZE)
                consoleLog.Dequeue();
            consoleLog.Enqueue(message);
            if (reader != null && settings != null && settings.saveLog)
                File.AppendAllLines($"{reader.mBigFileFolder}\\log.txt", new string[] { $"{message}"});
        }

        public static void InjectionLog(List<string> list)
        {
            if (reader != null && settings != null && settings.saveLog)
                File.AppendAllLines($"{reader.mBigFileFolder}\\log.txt", list);
            injectionLog = list;
        }

        public static bool LoadVBF(string path)
        {
            reader = new VirtuosBigFileReader();
            try
            {
                reader.LoadBigFileFile(path);
                Lab._popup.Message("Initializing virtual file system");
                fileSystem = new VFileSystem(path.Split('/').Last(), reader);
                return true;
            }
            catch (Exception e)
            {
                fileSystem = null;
                Log(e.ToString());
                return false;
            }
        }

    }
}
