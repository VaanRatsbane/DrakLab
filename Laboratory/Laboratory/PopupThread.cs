﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratory
{
    public class PopupThread
    {
        private PopupForm _popup;
        private Thread thread;

        public PopupThread()
        {
            _popup = null;
            Create();
        }

        private void Create()
        {
            if (_popup is null)
            {
                Control.CheckForIllegalCrossThreadCalls = false;
                _popup = new PopupForm();
                thread = _popup.RunInNewThread(false);
            }
        }

        public void ShowPopup(int ownX, int ownY, int ownW, int ownH)
        {
            Create();
            if (_popup.InvokeRequired)
            {
                _popup.Invoke(new MethodInvoker(() => ShowPopup(ownX, ownY, ownW, ownH)));
                return;
            }
            _popup.ShowPopup(ownX, ownY, ownW, ownH);
        }

        public void Message(string msg)
        {
            Create();
            if (_popup.InvokeRequired)
            {
                _popup.BeginInvoke(new MethodInvoker(() => Message(msg)));
                return;
            }
            _popup.Message(msg);
        }

        public void ClosePopup()
        {
            if (_popup.InvokeRequired)
            {
                _popup.Invoke(new MethodInvoker(() => ClosePopup()));
                return;
            }
            _popup.ClosePopup();
        }

        public void StartProgress(int max)
        {
            if (_popup.InvokeRequired)
            {
                _popup.Invoke(new MethodInvoker(() => StartProgress(max)));
                return;
            }
            _popup.StartProgress(max);
        }

        public void StepProgress(int step)
        {
            if (_popup.InvokeRequired)
            {
                _popup.BeginInvoke(new MethodInvoker(() => StepProgress(step)));
                return;
            }
            _popup.StepProgress(step);
        }

        public void EndProgress()
        {
            if (_popup.InvokeRequired)
            {
                _popup.Invoke(new MethodInvoker(() => EndProgress()));
                return;
            }
            _popup.EndProgress();
        }
    }
}
